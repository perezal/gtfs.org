---
document_order: 2

# stops.txt
table_data:
  stop_id:
    1: "Stops that are in different physical locations (i.e., different designated precise locations for vehicles on designated routes to stop, potentially distinguished by signs, shelters, or other such public information, located on different street corners or representing different boarding facility such as a platform or bus bay, even if nearby each other) should have different `stop_id`. <!-- (27) -->"
    2: "`stop_id` is an internal ID, not intended to be shown to passengers. <!-- (38) -->"
    3: "Maintain consistent `stop_id` for the same stops across data iterations (see <a href="#publishing">Dataset Publishing & General Practices</a>). <!-- (28) -->"
    10: "An extra loop! Something new! Is there a commit? I doubt it!"
  stop_name:
    4: "The `stop_name` should match the agency's public name for the stop, station, or boarding facility, e.g. what is printed on a timetable, published online, and/or presented at the location. <!-- (29) -->"
    5: "When there is not a published stop name, follow consistent stop naming conventions throughout the feed. <!-- (30) -->"

    # problem - can't use colons in text
    # problem - must start recommendation with letter or html
    # problem - errors will kill entire table
    # nested tables fix - link to separate table
---

### stops.txt

<table class="recommendation">
  <thead>
    <tr>
      <th>Field Name</th>
      <th>#</th>
      <th>Recommendation</th>
    </tr>
  </thead>
  <tbody>
    {% for field_name in page.table_data %}
      {% for recommendation in field_name[1] %}
        <tr id="stops_{{ recommendation[0] }}" class="anchor-row">
          <td>{% if forloop.first == true %}<code>{{ field_name[0] }}</code>{% endif %}</td>
          <td>{{ recommendation[0] }}</td>
          <td>{{ recommendation[1] | markdownify }}</td>
        </tr>
      {% endfor %}
    {% endfor %}
  </tbody>
</table>
